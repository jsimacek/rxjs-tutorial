import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {concatMap, debounceTime, distinctUntilChanged, filter, map, scan, switchMap} from 'rxjs/operators';
import {combineLatest, merge, Observable, Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';

const API_KEY = 'Ckq0cz6uaRJ4KNB4bv7o7KqzcGXksnaMLaeE3CnG';
const NASA_API = 'https://api.nasa.gov/neo/rest/v1';
const ONE_RANDOM_NEO = '3727662';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  private today: string = new Date().toISOString().substr(0, 10);

  public firstNeoDetail$: Observable<any>;
  public bothApis$: Observable<any>;
  public actionObservable$: Subject<number> = new Subject<number>();
  public actionObservableResult$: Observable<number>;

  public multipleInputsValue$: Observable<number>;

  public parallelRequestsResult$: Observable<[any[], any]>;

  public form: FormGroup;

  constructor(private http: HttpClient,
              private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  public requestChaining(): void {
    const neoFeed$ = this.http.get(`${NASA_API}/feed?start_date=${this.today}&end_date=${this.today}&api_key=${API_KEY}`);
    this.firstNeoDetail$ = neoFeed$.pipe(
      map((feed: any) => feed['near_earth_objects'][this.today]),
      switchMap((neos: any[]) => this.http.get(`${NASA_API}/neo/${neos[0].id}?api_key=${API_KEY}`))
    );
  }

  public bothRequests(): void {
    const neoFeed$ = this.http.get(`${NASA_API}/feed?start_date=${this.today}&end_date=${this.today}&api_key=${API_KEY}`).pipe(
      map((feed: any) => feed['near_earth_objects'][this.today])
    );

    this.bothApis$ = neoFeed$.pipe(
      concatMap((neos: any[]) => {
        return this.http.get(`${NASA_API}/neo/${neos[0].id}?api_key=${API_KEY}`)
          .pipe(map((detail => [detail, neos])));
      }),
      map(([detail, neos]: [any, any[]]) => ({firstObjectName: detail['name'], noOfObjects: neos.length}))
    );
  }

  public parallelRequests(): void {
    const neoFeed$ = this.http.get(`${NASA_API}/feed?start_date=${this.today}&end_date=${this.today}&api_key=${API_KEY}`).pipe(
      map((feed: any) => feed['near_earth_objects'][this.today])
    );
    const detail$ = this.http.get(`${NASA_API}/neo/${ONE_RANDOM_NEO}?api_key=${API_KEY}`);

    this.parallelRequestsResult$ = combineLatest([neoFeed$, detail$]);
  }

  public ngOnInit(): void {
    this.actionObservableResult$ = this.actionObservable$.pipe(
      scan((acc, curr) => acc + curr, 0)
    );

    this.setupForm();
    this.multipleInputsValue$ = merge(
      this.activatedRoute.queryParams.pipe(
        map(params => params['testParam']),
        filter(testParam => !!testParam),
        map(testParam => parseInt(testParam.toString(), 10))
      ),
      this.form.valueChanges.pipe(
        map(values => values['testInput']),
        debounceTime(200),
        distinctUntilChanged()
      )
    );
  }

  private setupForm(): void {
    this.form = this.formBuilder.group({testInput: this.formBuilder.control('')});
  }
}
